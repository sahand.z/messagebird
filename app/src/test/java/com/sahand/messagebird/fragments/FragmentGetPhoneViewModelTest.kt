package com.sahand.messagebird.fragments

import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import com.sahand.messagebird.model.GetPhoneRepository
import com.sahand.messagebird.model.IGetPhoneRepo
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.schedulers.TestScheduler
import kotlinx.coroutines.delay
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit


class FragmentGetPhoneViewModelTest {
    lateinit var ts: TestScheduler
    @Before
    fun before(){
        MockKAnnotations.init(this)
    }

    @After
    fun after(){
        unmockkAll()
    }

    @RelaxedMockK
    lateinit var repo: IGetPhoneRepo

    fun createViewModel() = FragmentGetPhoneViewModel(repo)

    @Test
    fun `when phone number looked up, put it in single live event`() {
        every { repo.lookup(any()) } returns Single.just(LookupResponseModel())
        val vm = createViewModel()
        ts = TestScheduler()
        ts.advanceTimeBy(3000,TimeUnit.MILLISECONDS)
        ts.triggerActions()
        repo.lookup("!23").subscribeOn(ts).subscribe({
            vm.lookup("98","9163616522",subscribeScheduler = ts,observeScheduler = ts)

            assertEquals(LookupResponseModel(),vm.lookUpResponse.value)

        },{

        })
    }


}