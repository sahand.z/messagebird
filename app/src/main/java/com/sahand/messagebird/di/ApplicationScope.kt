package com.sahand.messagebird.di

import javax.inject.Scope

@Scope
@Retention
annotation class ApplicationScope