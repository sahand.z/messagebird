package com.sahand.messagebird.di

import com.sahand.messagebird.model.GetPhoneRepository
import com.sahand.messagebird.model.IGetPhoneRepo
import com.sahand.messagebird.tools.INetworkTools
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideRepository(networkTools: INetworkTools): IGetPhoneRepo {
        return GetPhoneRepository(networkTools)
    }

}
