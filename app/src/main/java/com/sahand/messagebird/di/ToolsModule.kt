package com.sahand.messagebird.di

import android.app.Application
import com.sahand.messagebird.tools.INetworkTools
import com.sahand.messagebird.tools.NetworkTools
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ToolsModule {

    @Provides
    @Singleton
    fun provideNetworkTools(app: Application): INetworkTools {
        return NetworkTools()
    }

}