package com.sahand.messagebird.di

import android.app.Application
import com.sahand.messagebird.model.IGetPhoneRepo
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

//@ApplicationScope
@Singleton
@Component(modules = [ToolsModule::class, AppModule::class])
interface AppComponent{

    fun inject(application: Application)
    fun getPlacesRepository(): IGetPhoneRepo
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun injectApp(app: Application):Builder
        fun build():AppComponent
    }

}
