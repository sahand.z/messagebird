package com.sahand.messagebird.di

import com.bazaar.placesprovider.IMessagBirdAPIProvider
import dagger.Component

@ApplicationScope
@Component(modules = [PlacesModule::class])
interface PlacesComponent{

    fun getPlacesProvider(): IMessagBirdAPIProvider


}
