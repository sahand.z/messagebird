package com.sahand.messagebird.di

import com.bazaar.placesprovider.IMessagBirdAPIProvider
import com.sahand.messagebird.apiprovider.APIProvider
import dagger.Module
import dagger.Provides

@Module
class PlacesModule{

    @ApplicationScope
    @Provides
    fun providePlacesProvider(): IMessagBirdAPIProvider {
        return APIProvider()
    }

}
