package com.bazaar.placesprovider

import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

interface IMessagBirdAPIProvider {

    /**
     * This function calls remote API to get places around a location*/
    fun lookupPhone(phone: String): Single<LookupResponseModel>

    /**
     * This function call remote API to get a place's details*/
}
