package com.sahand.messagebird.apiprovider.di

import com.sahand.messagebird.apiprovider.remoteapi.IMessgaBirdAPI
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [APIModule::class, APIConfig::class])
interface APIComponent {
    fun getAPI(): IMessgaBirdAPI
}