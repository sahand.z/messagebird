package com.sahand.messagebird.apiprovider.pojos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

public data class Formats (
    @SerializedName("e164")
    @Expose
    var e164: String? = null,
    @SerializedName("international")
    @Expose
    var international: String? = null,
    @SerializedName("national")
    @Expose
    var national: String? = null,
    @SerializedName("rfc3966")
    @Expose
    var rfc3966: String? = null)
