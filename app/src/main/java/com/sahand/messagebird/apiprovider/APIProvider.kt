package com.sahand.messagebird.apiprovider

//import com.bazaar.placesprovider.di.DaggerAPIComponent
import com.bazaar.placesprovider.IMessagBirdAPIProvider
import com.sahand.messagebird.apiprovider.di.DaggerAPIComponent
import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import com.sahand.messagebird.apiprovider.remoteapi.IMessgaBirdAPI
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response


class APIProvider(): IMessagBirdAPIProvider
{
    override fun lookupPhone(phone: String): Single<LookupResponseModel> {
        return apiClient.lookupPhone(phoneNumber = phone)
    }

    var  apiClient: IMessgaBirdAPI = DaggerAPIComponent.create().getAPI()

}