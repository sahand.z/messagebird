package com.sahand.messagebird.apiprovider.di

import com.sahand.messagebird.apiprovider.remoteapi.IMessgaBirdAPI
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class APIModule {

    @Provides
    @Singleton
    fun proviceBTCAPI(retrofit: Retrofit): IMessgaBirdAPI {
        return retrofit.create(IMessgaBirdAPI::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        client: OkHttpClient,
        rxConverter: RxJava2CallAdapterFactory,
        gson: GsonConverterFactory,
        baseURL: String
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseURL)
            .addCallAdapterFactory(rxConverter)
            .addConverterFactory(gson)
            .client(client)
            .build()
    }

    @Provides
    @Singleton
    fun provideRXConverter(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideGsonConverter(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideOKHttpClient(
        interceptor: Interceptor
    ): OkHttpClient {

        var clientBuilder = OkHttpClient().newBuilder()

//        if (debugMode)
        return clientBuilder.addInterceptor(interceptor).build()
//        return clientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideOkhttpLogger(): Interceptor {

        var logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY
        return logger
    }


}