package com.sahand.messagebird.apiprovider.remoteapi

import com.sahand.messagebird.BuildConfig
import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface IMessgaBirdAPI {

    @GET("lookup/{PHONE}")
    fun lookupPhone(@Header("Authorization") token:String = BuildConfig.api_key, @Path("PHONE") phoneNumber:String):Single<LookupResponseModel>
}