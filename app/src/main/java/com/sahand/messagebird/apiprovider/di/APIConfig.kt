package com.sahand.messagebird.apiprovider.di

import android.app.Application
import android.content.Context
import android.os.Build
import android.os.Debug
import com.sahand.messagebird.BuildConfig
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class APIConfig {

    val readTimeout:Long     = 30000
    val writeTimeout:Long    = 30000
    val timeout:Long         = 30000
    val baseURL:String       = "https://rest.messagebird.com/"

    @Provides
    @Singleton
    fun provideBaseURL():String{
        return baseURL
    }

    @Named("debugMode")
    @Provides
    @Singleton
    fun provideDebugMode():Boolean{
        return BuildConfig.DEBUG
    }

    @Named("readTO")
    @Provides
    @Singleton
    fun provideReadTimeout():Long{
        return readTimeout
    }

    @Named("writeTO")
    @Provides
    @Singleton
    fun provideWriteTimeout():Long{
        return writeTimeout
    }

    @Named("callTO")
    @Provides
    @Singleton
    fun provideCallTO():Long{
        return timeout
    }


}
