package com.sahand.messagebird.apiprovider.pojos

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

data class LookupResponseModel(
        @SerializedName("href")
        @Expose
        var href: String? = null,
        @SerializedName("countryCode")
        @Expose
        var countryCode: String? = null,
        @SerializedName("countryPrefix")
        @Expose
        var countryPrefix: String? = null,
        @SerializedName("phoneNumber")
        @Expose
        var phoneNumber: String? = null,
        @SerializedName("type")
        @Expose
        var type: String? = null,
        @SerializedName("formats")
        @Expose
        var formats: Formats? = null
)