package com.sahand.messagebird.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.sahand.messagebird.R
import kotlinx.android.synthetic.main.details_fragment.*

class DetailsFragment : Fragment() {

    companion object {
        fun newInstance() = DetailsFragment()
    }

    private lateinit var viewModel: DetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        arguments?.let {
            viewModel.reportIntentValues(type = DetailsFragmentArgs.fromBundle(it).type,href = DetailsFragmentArgs.fromBundle(it).href, countryCode = DetailsFragmentArgs.fromBundle(it).countryCode, phoneNumber= DetailsFragmentArgs.fromBundle(it).phoneNumber
            ,e164 = DetailsFragmentArgs.fromBundle(it).e164,international= DetailsFragmentArgs.fromBundle(it).international,national = DetailsFragmentArgs.fromBundle(it).national, rfc3966 = DetailsFragmentArgs.fromBundle(it).rfc3966)


        }
        viewModel.lookupResponseLivedata.observe(viewLifecycleOwner, Observer {
            Log.e("typeeee",it.type + " hoooooooooooooooraaaaaaaaay")
            type.text = "Type: " + it.type
            href.text = "Href: " + it.href
            countryCode.text ="CountryCode: " +  it.countryCode
            countryPrefix.text= "CountryPrefix: " + it.countryPrefix
            phoneNumber.text ="phoneNumber: " + it.phoneNumber
            e164.text = "E164: " +  it.formats?.e164
            international.text = "International: " + it.formats?.international
            national.text ="national: " +  it.formats?.national
            rfc3966.text ="Rfc3966: " +  it.formats?.rfc3966

        })
    }


}