package com.sahand.messagebird.fragments

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import com.sahand.messagebird.di.DaggerAppComponent
import com.sahand.messagebird.model.IGetPhoneRepo
import com.sahand.messagebird.tools.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class FragmentGetPhoneViewModel(private var repository: IGetPhoneRepo) : ViewModel() {
    public var lookUpResponse: SingleLiveEvent<LookupResponseModel> = SingleLiveEvent()
    public var lookUpError: SingleLiveEvent<String> = SingleLiveEvent()
    fun lookup(selectedCountryCode: String?, phone: String,
               observeScheduler: Scheduler ,
               subscribeScheduler: Scheduler ) {
        repository.lookup(selectedCountryCode+phone).subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
            .subscribe(Consumer {
                lookUpResponse.value = it
            }, Consumer {
                //todo move hard coded string to strings file
                lookUpError.value = "error in loading phone number"
            })

    }

    sealed class APIResponseGetPhone(){


    }
}