package com.sahand.messagebird.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.hbb20.CountryCodePicker
import com.sahand.messagebird.R
import com.sahand.messagebird.di.DaggerAppComponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class FragmentGetPhone : Fragment() {

    companion object {
        fun newInstance() = FragmentGetPhone()
    }

    private lateinit var viewModel: FragmentGetPhoneViewModel
    private lateinit var countryCodePicker: CountryCodePicker
    private lateinit var edtPhone: EditText
    private lateinit var btnLookup: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_get_phone, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this, activity?.application?.let {
            DaggerAppComponent.builder().injectApp(
                it
            ).build().getPlacesRepository()
        }?.let { GetPhoneModelFactory(it) }).get(FragmentGetPhoneViewModel::class.java)

        viewModel.lookUpResponse.observe(viewLifecycleOwner, Observer {
            val action =
                FragmentGetPhoneDirections.actionFragmentGetPhoneToDetailsFragment(
                    it.type,
                    it.href,
                    it.countryCode,
                    it.phoneNumber,
                    it.formats?.e164,
                    it.formats?.international,
                    it.formats?.national,
                    it.formats?.rfc3966
                )
            btnLookup.isEnabled = true
            Navigation.findNavController(view).navigate(action);
        })
        viewModel.lookUpError.observe(viewLifecycleOwner, Observer {
            Toast.makeText(activity,it,Toast.LENGTH_LONG).show()
        })


        countryCodePicker = view.findViewById(R.id.countryCodePicker)
        edtPhone = view.findViewById(R.id.edtNumber)
        btnLookup = view.findViewById(R.id.btnLookup)
        btnLookup.setOnTouchListener(View.OnTouchListener { v, event ->
            btnLookup.isEnabled = false
            viewModel.lookup(countryCodePicker.selectedCountryCode, edtPhone.text.toString(),AndroidSchedulers.mainThread(),Schedulers.io())

            true
        })


    }

    override fun onStop() {
        super.onStop()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}