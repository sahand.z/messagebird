package com.sahand.messagebird.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sahand.messagebird.model.IGetPhoneRepo

class GetPhoneModelFactory(var repo: IGetPhoneRepo): ViewModelProvider.Factory{


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FragmentGetPhoneViewModel::class.java)) {
            return FragmentGetPhoneViewModel(repo) as T
        }
        throw IllegalArgumentException("unknown viewmodel")
    }


}