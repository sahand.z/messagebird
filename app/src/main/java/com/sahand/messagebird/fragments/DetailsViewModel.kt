package com.sahand.messagebird.fragments

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sahand.messagebird.apiprovider.pojos.Formats
import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel

class DetailsViewModel : ViewModel() {
    val lookupResponseLivedata: MutableLiveData<LookupResponseModel> = MutableLiveData()
    fun reportIntentValues(
        type: String?,
        href: String?,
        countryCode: String?,
        phoneNumber: String?,
        e164: String?,
        international: String?,
        national: String?,
        rfc3966: String?
    ) {
        lookupResponseLivedata.postValue(LookupResponseModel(href= href,countryCode = countryCode,phoneNumber=phoneNumber
                , type = type,formats = Formats(e164 = e164, international = international, national = national, rfc3966 =rfc3966)
        ))
    }


    // TODO: Implement the ViewModel
}