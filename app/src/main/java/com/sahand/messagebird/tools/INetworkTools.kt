package com.sahand.messagebird.tools

import android.app.Application

interface INetworkTools {
    fun hasNetwork(app: Application):Boolean?
}
