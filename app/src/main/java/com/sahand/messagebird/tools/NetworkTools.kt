package com.sahand.messagebird.tools

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class NetworkTools: INetworkTools {

    /**
     * This function returns wheather the application has internet or not
     * */
    override fun hasNetwork(app: Application): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }
}