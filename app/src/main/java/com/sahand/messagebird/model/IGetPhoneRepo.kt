package com.sahand.messagebird.model

import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

interface IGetPhoneRepo {
    fun lookup(phone:String): Single<LookupResponseModel>
}
