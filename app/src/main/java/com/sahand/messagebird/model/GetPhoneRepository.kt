package com.sahand.messagebird.model

import com.sahand.messagebird.apiprovider.pojos.LookupResponseModel
import com.sahand.messagebird.di.DaggerPlacesComponent
import com.sahand.messagebird.tools.INetworkTools
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

class GetPhoneRepository(var networkTools: INetworkTools):
    IGetPhoneRepo {
    override fun lookup(phone: String ): Single<LookupResponseModel> {
        return DaggerPlacesComponent.builder().build().getPlacesProvider().lookupPhone(phone = phone)
    }



}
