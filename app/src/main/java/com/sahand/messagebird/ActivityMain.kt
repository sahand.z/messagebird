package com.sahand.messagebird

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import com.sahand.messagebird.di.DaggerAppComponent
import com.sahand.messagebird.fragments.FragmentGetPhone
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ActivityMain: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

    }

    override fun onBackPressed() {
        try {
            Navigation.findNavController(this,R.id.navigation_main).navigateUp()

        }catch (e: Exception){

        }
        super.onBackPressed()
    }
}